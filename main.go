package main

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	_ "github.com/lib/pq"
	"log"
	"math/big"
	"runtime"
	"time"
)

type Ethereum struct {
	eth    *ethclient.Client
	ticker *time.Ticker
	done   chan bool
}

func main() {
	runtime.GOMAXPROCS(2)
	ethereum := &Ethereum{}
	var err error
	ethereum.eth, err = ethclient.Dial("http://0.0.0.0:8545")
	if err != nil {
		log.Fatal(err)
	}

	ethereum.done = make(chan bool)
	ethereum.ticker = time.NewTicker(1 * time.Second)

	go ethereum.listen()

	select {}
}

func (eth *Ethereum) listen() {

	var isFirst bool
	isFirst = true
	var block big.Int
	for {
		select {

		case <-eth.done:
			return

		case <-eth.ticker.C:
			if isFirst {
//				fmt.Println("Первый раз")
				header, err := eth.eth.BlockByNumber(context.Background(), nil)
				if err != nil {
					fmt.Println(err)
					continue
				} else {
					if header != nil {
						block = *header.Number()
						go Save(*header)
//						fmt.Println(header.Number().String())
						isFirst = false
					}
				}
			} else {
				header, err := eth.eth.BlockByNumber(context.Background(), nil)
				if err != nil {
					fmt.Println(err)
					continue
				}
				if header != nil {
					if header.Number().Int64() > block.Int64() {
						block = *header.Number()
						go Save(*header)
//						fmt.Println(header.Number().String())
					}
				}
			}
		}
	}
}

func Save(block types.Block) {

	ks := keystore.NewKeyStore("C:/Users/mekannepes/Desktop/ethereum/rinkeby/keystore", keystore.StandardScryptN, keystore.StandardScryptP)

	wallets := ks.Accounts()

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()
	fmt.Println()
	fmt.Println("Tx count: ",len(block.Transactions()))
	fmt.Println("Block number: ",block.Number())
	for _, tx := range block.Transactions() {
		fmt.Println(tx.To().Hex())
		addressFromTx := tx.To().Hex()
		for _, w := range wallets {
			addressFromKS := w.Address.Hex()
			if addressFromTx == addressFromKS {
				db, _ := sql.Open("postgres", "user=postgres password=mekannepes dbname=ethereum sslmode=disable")
				fmt.Println("DEN")

				var id int

				row := db.QueryRow("select id from wallets where address = $1", w.Address.Hex())
				err := row.Scan(&id)
				if err != nil {
					break
				}

				_, err = db.Exec("insert into receivedtransactions(wallet_id, hash) values($1,$2)", id, tx.Hash().Hex())
				if err != nil {
					fmt.Println(err)
					break
				}

				db.Close()

				break
			} else {
				continue
			}
		}
	}
}
